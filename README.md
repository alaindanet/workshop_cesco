# workshop_cesco

This is minimal example

# Clone the repo:

```
git clone https://framagit.org/alaindanet/workshop_cesco.git
```

# Check the repo:

```
cd workshop_cesco
git status
git log
```

# Set a local remote:

## Create an empty git repo 

```
cd ..
git init remote
```

## Set connection to the remote

```
cd ..
cd workshop_cesco
git remote -v
git remote add local ~/remote/ 
git remote -v
```

## Push your repo to your new remote 

```
git push local master
```

Congrats!


# Experiment a conflict

What does happen if your current repo and the remote have modified the same line
of the text file ?

1. Make some modifications in the README.md
2. Go to your local remote
  - `cd ..`
  - `cd remote`
3. Modify the same line in the README.md
4. git add, commit
5. cd ..
6. cd workshop_cesco
7. git pull local master
